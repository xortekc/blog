from main import app
import config
from paste import reloader
reloader.install()


def main():
    from paste import httpserver
    httpserver.serve(app, host=config.host, port=config.port)


if __name__ == '__main__':
    main()
