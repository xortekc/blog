import webapp2
import jinja2
import os
import mimetypes


JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)+'/templates'),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


class StaticFileHandler(webapp2.RequestHandler):
    def get(self, path):
        abs_path = os.path.abspath(os.path.join('./static', path))
        if os.path.isdir(abs_path) or abs_path.find(os.getcwd()) != 0:
            self.response.set_status(403)
            return
        try:
            f = open(abs_path, 'r')
            content_type = mimetypes.guess_type(abs_path)[0]
            self.response.headers['Content-Type'] = content_type
            self.response.out.write(f.read())
            f.close()
        except:
            self.response.set_status(404)


class BaseHandler(webapp2.RequestHandler):
    def render(self, template, **params):
        t = JINJA_ENVIRONMENT.get_template(template)
        self.response.write(t.render(params))


class MainPage(BaseHandler):
    def get(self):
        self.render("index.html")


app = webapp2.WSGIApplication([
    ('/static/(.+)', StaticFileHandler),
    ('/', MainPage),
], debug=True)
