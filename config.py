ENV = 'dev'

if ENV == 'dev':
    host = '127.0.0.1'
    port = '80'
elif ENV == 'uat':
    host = 'xoralex.tk'
    port = '80'
elif ENV == 'prod':
    host = 'xoralex.com'
    port = '80'
